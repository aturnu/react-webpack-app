import ReactDOM from "react-dom";
import React from "react";

import App from "./App";

class CustomElement extends HTMLElement {
  connectedCallback() {
    const mountPoint = document.createElement("span");
    this.attachShadow({ mode: "open" }).appendChild(mountPoint);

    const name = this.getAttribute("name");
    ReactDOM.render(<App name={name} />, mountPoint);
  }
}
customElements.define("react-html-tag", CustomElement);
