const path = require("path");
const PACKAGE = require('./package.json');
const version = PACKAGE.version;

module.exports = (webpackConfigEnv, argv) => {
  return _webpackConfig({
    orgName: "entando",
    projectName: "build-bundle",
    webpackConfigEnv,
    argv,
  });
};

function _webpackConfig(opts) {
  let argv = opts.argv || {};

  let isProduction = argv.p || argv.mode === "production";

  return {
    mode: isProduction ? "production" : "development",
    entry: path.resolve(
      process.cwd(),
      `src/index.js`
    ),
    output: {
      filename: `build.js`,
      libraryTarget: "umd",
      path: path.resolve(process.cwd(), "dist"),
      uniqueName: opts.projectName,
      devtoolNamespace: `${opts.projectName}`,
      publicPath: "",
    },
    externals: {
      "react-dom": "ReactDOM",
      react: "React",
    },
    module: {
      rules: [
        {
          test: /\.(js|ts)x?$/,
          exclude: /node_modules/,
          use: {
            loader: require.resolve("babel-loader", { paths: [__dirname] }),
          },
        },
        {
          test: /\.css$/i,
          include: [/node_modules/, /src/],
          use: [
            {
              loader: require.resolve("style-loader", { paths: [__dirname] }),
            },
            {
              loader: require.resolve("css-loader", { paths: [__dirname] }),
              options: {
                modules: false,
              },
            },
          ],
        },
        {
          test: /\.(bmp|png|svg|jpg|jpeg|gif|webp)$/i,
          exclude: /node_modules/,
          type: "asset/resource",
        },
        {
          test: /\.html$/i,
          exclude: /node_modules/,
          type: "asset/source",
        },
      ],
    },
    devtool: "source-map",
    devServer: {
      compress: true,
      historyApiFallback: true,
      headers: {
        "Access-Control-Allow-Origin": "*",
      },
      firewall: false,
      static: path.join(__dirname, "public"),
      client: {
        host: "localhost",
      },
    },
    resolve: {
      extensions: [".mjs", ".js", ".jsx", ".wasm", ".json"],
    },
  };
}
