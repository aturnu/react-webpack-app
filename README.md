## react-webpack-app
A boiler plate for creating react applications bundled by webpack (using ES6+, Babel and webpack development server)

# Quickstart

Make sure to have [Node.js](https://nodejs.org/it/) installed.
```
git clone https://gitlab.com/aturnu/react-webpack-app.git
```
Optional `git remote remove origin`, then:
```
cd react-webpack-app
npm i

```

 
 To start the app type:
```
npm start
```
 The project will run on `localhost:/8084`. To change the port edit `scripts.start` on `package.json`.
 

## Build
The app is bundled and builded using [webpack](https://webpack.js.org/). Type the following command on the terminal:
```
npm run build
```
Build files will be located into `dist` folder.

